const { override, addLessLoader, addWebpackAlias } = require('customize-cra');
const path = require('path');

module.exports = override(
  addLessLoader(),
  addWebpackAlias({
    ['@']: path.resolve(__dirname, './src')
  })
);
