import { render, screen, fireEvent } from '@testing-library/react';

import CollapseBox from '../components/CollapseBox';

/**
 * the unit test for CollapseBox component
 */

describe('<CollapseBox />', () => {
  test('should render title: Hello', () => {
    render(<CollapseBox title='Hello' />);
    const title = screen.getByText(/Hello/i);
    expect(title).toBeInTheDocument();
  });

  test('initialization CollapseBox showed content', () => {
    render(<CollapseBox title='Hello' />);
    const wrapper = screen.getByTestId('wrapper');
    expect(wrapper.className).toMatch(/show/);
  });

  test('click toggle arrow should be effective', () => {
    render(<CollapseBox title='Hello' />);
    const wrapper = screen.getByTestId('wrapper');
    const toggle = screen.getByTestId('toggle');

    expect(wrapper.className).toMatch(/show/);
    fireEvent.click(toggle);
    expect(wrapper.className).toMatch(/close/);
  });
});
