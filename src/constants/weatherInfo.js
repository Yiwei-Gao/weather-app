/**
 * a mock weather data including city, temperature, time,
 * humidity, chance of rain, wind speed and tomorrow temperature.
 */
export const WEATHER__INFO = {
    CITY: "Melbourne",
    TEMPERATURE: 32,
    TIME: "Tue 16th 3:46 PM",
    HUMIDITY: "78%",
    CHANCE_OF_RAIN: "34%",
    WIND_SPEED: 21,
    TOMORROW_TEMPERATURE: 30,
}