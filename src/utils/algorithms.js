/**
 * 
 * @param {Array} arr algorithm list 
 * @param {string} property the property of the algorithm object
 * @returns the sum total of the values in the algorithm list
 */

function getSum(arr, property) {
    return arr.reduce((sum ,currentValue) => {
        sum += currentValue[property];
        return sum;
    }, 0);
}

/**
 * 
 * @param {Array} arr algorithm list
 * @returns a sorted algorithm list contains a list of value and corresponding percentage
 */
const calculate = arr => {
    // get the sum of the values in the algorithm list.
    const sum = getSum(arr, 'value');
    // iterate the item in the algorithm list, 
    // calculate the percentage of each value and assign it to each item in the algorithm list
    arr.forEach((item, index) => {
        // if the last item, calculate the sum of all the other items' percentage 
        // and assign the rest of the percentage to last item,
        // in order to make sure the sum of percentage is 100%
        if(index === arr.length - 1) {
            const prev = getSum(arr.slice(0, arr.length - 1), 'percentage');
            item.percentage = 100 - prev; 
        } else {
            // if is not the last item, calculate it percentage by dividing value by sum
            item.percentage = Math.round((item.value / sum) * 100);
        }
    });
    // sort the array by item's value
    arr.sort((a, b) => a.value - b.value);
    return arr;
};

/**
 * 
 * @param {number} num the length of array
 * @returns a function to do calculation for the list 
 */
function getRampAlgorithms(num) {
    // create an array with length is equal to num,
    // and assign a random number to value field
    let list = new Array(num).fill('algorithm').map(() => {
        return {
            value: Math.floor(Math.random() * 100)
        };
    });
    return calculate(list);
};

/**
 * 
 * @param {number} num the length of algorithm list
 * @returns an algorithm map, key is algorithm id, value is the calculated result
 */
export function createAlgorithms(num) {
    let algorithms = new Map();
    for(let i = 1; i < num; i++) {
        const value = getRampAlgorithms(num)
        algorithms.set(`Algorithm ${i}`, value);
    }
    
    return algorithms;
}