import React from 'react';
import DelayedRoutes from './components/DelayedRoutes';
import RampChart from './components/RampChart';
import WeatherInfo from './components/WeatherInfo';
import './index.less';

/**
 * 
 * @returns side bar component
 */

function SideBar() {
    return (
        <div className='sidebar-container'>
            <WeatherInfo />
            <DelayedRoutes />
            <RampChart />
        </div>
    )
}

export default SideBar
