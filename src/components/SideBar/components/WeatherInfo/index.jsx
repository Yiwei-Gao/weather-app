import React from "react";
import cloudyToClear from "../../../../assets/cloudy-to-clear.png";
// import mock weather data 
import { WEATHER__INFO } from '../../../../constants/weatherInfo'

function WeatherInfo() {
  return (
    <div className="weather-info">
      <div className="flex-box">
        <div className="item">
          <div className="city">{WEATHER__INFO.CITY}</div>
          <div className="temperature">{WEATHER__INFO.TEMPERATURE}&#176;</div>
          <div className="time">{WEATHER__INFO.TIME}</div>
        </div>

        <div className="item">
          <img src={cloudyToClear} alt="cloudyToClear" />
        </div>
      </div>
      <div className="list">
        <div className="flex-box">
          <span className="item">Humidity</span>
          <span className="value">{WEATHER__INFO.HUMIDITY}</span>
        </div>

        <div className="flex-box">
          <span className="item">Chance of Rain</span>
          <span className="value">{WEATHER__INFO.CHANCE_OF_RAIN}</span>
        </div>

        <div className="flex-box">
          <span className="item">Wind</span>
          <span className="value">{WEATHER__INFO.WIND_SPEED} km/h</span>
        </div>

        <div className="flex-box">
          <span className="item">Tomorrow</span>
          <span className="value">{WEATHER__INFO.TOMORROW_TEMPERATURE}&#176;</span>
        </div>
      </div>
    </div>
  );
}

export default WeatherInfo;
