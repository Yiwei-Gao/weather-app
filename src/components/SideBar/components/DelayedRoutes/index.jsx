import React from "react";
import CollapseBox from "../../../CollapseBox";
// import Delayed routes data
import { DELAYED_ROUTES } from '../../../../constants/delayedRoutes';

/**
 * 
 * @returns DelayedRoutes component
 */

const DelayedRoutes = () => {
  return (
      <CollapseBox
        title='DELAYED ROUTES' // the title of Delayed Routes passing to the CollapseBox
      >
          <div className='delayed-routes'>
              {/* iterate the element in the delayed routes array and render it */}
              {DELAYED_ROUTES.map((item, index) => ( 
                  <div key={index} className='item'>
                    <div className='flex-box'>
                        <div className='left'>
                            <span className={`dot ${item.type}`}></span>
                            <span className='start'>{item.routes[0]}</span>
                        </div>
                        <div className="distance">{item.distance} km</div>
                    </div>
                    <div className='flex-box'>
                        <i className='iconfont icon-arrow-down2'></i>
                        <div className='left flex'>
                            <div className='toList'>
                                <span className='to2'>{item.routes[1]}</span>
                                <span className='to2'>{item.routes[2]}</span>
                            </div>
                        </div>
                        <div>
                            <span className='time'>{item.time}</span> min
                        </div>
                    </div>
                  </div>
              ))}
          </div>
      </CollapseBox>
  )
};

export default DelayedRoutes;