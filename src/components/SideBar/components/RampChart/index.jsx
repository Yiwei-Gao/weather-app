import React, { useEffect } from 'react';
// import echarts for the creation of ramp chart
import * as echarts from 'echarts';
import 'echarts/lib/chart/pie'; 
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markPoint';
import CollapseBox from '../../../CollapseBox';
import { createAlgorithms } from '../../../../utils/algorithms';
import { getRampAlgorithms } from '../../../../services/api';

const RampChart = () => {

  useEffect(() => {
    // initialize the algorithm list
    const algorithmList = createAlgorithms(6);
    // prepare a DOM container for echarts
    const chartDom = document.getElementsByClassName("ring")[0];
    // based on prepared DOM, initialize echarts instance
    const chart = echarts.init(chartDom);

    getRampAlgorithms((ramps) => {
        // select a ramp randomly from the ramps list 
        const ramp = ramps[Math.floor(Math.random() * 50)];
        // get the algorithm corresponding to the selected ramp
        const algorithm = algorithmList.get(ramp?.algorithm);
        // specify chart configuration item and data
        const option = {
          name: 'algorithm',
          tooltip: {
            trigger: 'item'
          },
  
          color: ["#6ed8d0", "#2ab2a9", "#6ed8d3", "#f2f8fd", "#bcbcbc"],
          series: [
            {
              type: 'pie',
              radius: ['50%', '80%'],
              labelLine: {
                normal: {
                  show: false
                }
              },
              data: algorithm?.map(item => ({
                value: item.value,
                name: `${item.percentage}%`
              })),
              emphasis: {
                itemStyle: {
                  shadowBlur: 10,
                  shadowOffsetX: 0,
                  shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
              }
            }
          ]
        }
        // use configuration item and data specified to show chart
        chart.setOption(option);
    })
  }, [])

  return (
    <CollapseBox title="RAMP CHART" className="ramp-chart">
      <div >
        <div className="ring"></div>
        <div className="desc">Ramp algorithm blablabla</div>
      </div>
    </CollapseBox>
  );
};

export default RampChart;