import React, { useState } from 'react';
/**
 * 
 * @param {*} param0 
 * @returns Collapse Box component to show and hide the elements inside it
 */
const CollapseBox = ({ defaultShow = true, title, ...props}) => {
    // the status of collapse box, show the content by default
    const [show, setShow] = useState(defaultShow);
    // the class names of collapse box
    // when the status show is true, display the content, otherwise, hide it.
    const classes = `collapse-box ${show? 'show' : 'close'} ${props.className || ''}`;

    return (
        <div className={classes} data-testid='wrapper'>
            <div className='collapse-box-header'>
                <span className='title'>{title}</span>
                <i 
                    className='iconfont icon-arrow-down' 
                    onClick={() => setShow(prev => !prev)} // close or hide element by clicking the icon
                    data-testid="toggle">              
                </i>
            </div>
            {/* display the elements inside the CollapseBox */}
            <div className='collapse-content'>
                {props.children}
            </div>
        </div>
    )
}   

export default CollapseBox
